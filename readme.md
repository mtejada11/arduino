### Arduino Programming Exercises

1 - [ElecMB_Exercise1.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise1.ino) - LEDs blink in sequence

2 - [ElecMB_Exercise2.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise2.ino) - LEDs blink in sequence according to one button input

3 - [ElecMB_Exercise4.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise3.ino) - LED moves according to two button input 

4 - [ElecMB_Exercise3.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise4.ino) - LEDs blink in sequence according to one button input, also print messages

5 - [ElecMB_Exercise5.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise5.ino) - Read photoresistor input and print to serial monitor 

6 - [ElecMB_Exercise6.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise6.ino) - LEDs blink according to photoresistor input 

7 - [ElecMB_Exercise7.ino](https://bitbucket.org/mtejada11/arduino/src/master/ElecMB_2017_11/ElecMB_Exercise7.ino) - Demostration with 9g micro servo motor

### Circuit Schematic
![Circuit Schematic](https://bytebucket.org/mtejada11/arduino/raw/master/ElecMB_2017_11/Circuit_1_Schematic.png)



### Circuit Breadboard Layout
![Circuit Breadboard Layout](https://bytebucket.org/mtejada11/arduino/raw/master/ElecMB_2017_11/Circuit_2_BreadboardLayout.png)